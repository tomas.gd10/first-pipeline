# CHANGELOG



## v0.2.4 (2024-04-08)

### Fix

* fix(deploy): add deploy pipeline new version due to prior error 2 ([`c09c3db`](https://gitlab.com/tomas.gd10/first-pipeline/-/commit/c09c3db358011e08ce5c6efe8d317fbbd7139416))


## v0.2.3 (2024-04-08)

### Fix

* fix(deploy): add deploy pipeline new version due to prior error 2 ([`3a205b4`](https://gitlab.com/tomas.gd10/first-pipeline/-/commit/3a205b4a5484078154a271c5cf310de2b53d5775))


## v0.2.2 (2024-04-08)

### Fix

* fix(deploy): add deploy pipeline new version due to prior error ([`67798c5`](https://gitlab.com/tomas.gd10/first-pipeline/-/commit/67798c57f75e9f8fd112a0cbc90a9991c3178d31))


## v0.2.1 (2024-04-08)

### Fix

* fix(deploy): add deploy pipeline ([`134f4d5`](https://gitlab.com/tomas.gd10/first-pipeline/-/commit/134f4d5df34423460574b234afa68af20f301060))


## v0.2.0 (2024-04-08)

### Feature

* feat(addition): add addition feature ([`24ce58e`](https://gitlab.com/tomas.gd10/first-pipeline/-/commit/24ce58e2e4833b7e4c3ad8ca00afe5870db44aab))


## v0.1.0 (2024-04-08)

### Feature

* feat(sr): add semantic release cd pipeline ([`a5b5dc1`](https://gitlab.com/tomas.gd10/first-pipeline/-/commit/a5b5dc1665107875df857d67ef1a0fd1a9f1604a))

### Unknown

* add semantic release config ([`89776a6`](https://gitlab.com/tomas.gd10/first-pipeline/-/commit/89776a6dd99487f5665a7c5ffca5bca517c2464d))

* Add pyproject.toml to init poetry ([`eecb9d7`](https://gitlab.com/tomas.gd10/first-pipeline/-/commit/eecb9d7e880025efe9b8ce8c6d0ea9d2bde2f08a))

* Update .gitlab-ci.yml file ([`009c6e8`](https://gitlab.com/tomas.gd10/first-pipeline/-/commit/009c6e8acf1ca6183a843d45ee4737009e27f5e6))

* Add new file ([`dc35946`](https://gitlab.com/tomas.gd10/first-pipeline/-/commit/dc3594678d130c58b6f49984e8331f461673956b))

* Initial commit ([`c83f2df`](https://gitlab.com/tomas.gd10/first-pipeline/-/commit/c83f2df42e915242775751a248836cde602da273))
